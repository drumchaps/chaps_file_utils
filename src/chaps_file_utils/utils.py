
#-*- coding: utf-8 -*-

from random import randint
import os

def random_file_line(file_path):
    """Gets a random line from a file.\
    """
    with open(file_path, "w+") as f:
        lines = f.readlines()
        return lines[ random.randint(0, len(f)) ].strip()
        pass
    pass

def remove_line_coincidence( path, search ):
    """
    """
    with open(accounts_path, "a+") as f:
        lines = f.readlines()
        delete = None
        for i, line in enumerate(lines):
            if account in line:
                delete = i
                break
        if delete:
            with open(accounts_path, "w")as f1:
                for i, line in enumerate(lines):
                    if i == delete:
                        continue
                    f1.write(line)
    pass

def lines_array(file_path):
    """Returns all the lines from a file in a list.\
    """
    with open(file_path, "r+") as f:
        return f.readlines()
    pass


def check_path_exists(file_path, create=None):
    try:
        os.stat( file_path )
        return True
    except Exception as e:
        if create:
            os.mkdir(file_path)
            return True
        return False
    pass

