from distutils.core import setup

setup(
    name="chaps_file_utils",
    version="0.0.1",
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/chaps_file_utils",
    packages  = [
        "chaps_file_utils",
    ],
    package_dir={'chaps_file_utils': 'src/chaps_file_utils'},
    #install_requires = ["requests",]
)


